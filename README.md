# The mean deviations of the Earth’s surface temperature.

Global Temperature Time Series. Data are included from the GISS Surface Temperature (GISTEMP) analysis and the global component of Climate at a Glance (GCAG).

### GISTEMP

Combined Land-Surface Air and Sea-Surface Water Temperature Anomalies [i.e. deviations from the corresponding 1951-1980 means]. Global-mean annual means from 1880 to 2016.

### GCAG

Global temperature anomaly data come from the Global Historical Climatology Network-Monthly (GHCN-M) data set and International Comprehensive Ocean-Atmosphere Data Set (ICOADS), which have data from 1880 to 2016. These two datasets are blended into a single product to produce the combined global land and ocean temperature anomalies (GCAG). The available timeseries of global-scale temperature anomalies are calculated with respect to the 20th century average.

### Visualization

The file static_visualization.html produces a visualization from static files. 

### Source

Global Temperature Time Series - [Data Hub](https://datahub.io/core/global-temp)
